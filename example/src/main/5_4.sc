def squareList(xs: List[Int]): List[Int] = xs match {
  case Nil     => List()
  case y :: ys => y * y :: squareList(ys)
}
def squareList2(xs: List[Int]): List[Int] =
  xs map (x => x * x)

squareList(List(2,3))
squareList2(List(2,3))
