object mergesort {
  //def mSort(xs: List[Int]): List[Int] = xs match {
  //  case List() => throw  new Error("sort on empty list")
  //  case List(x) => xs
  //  case y :: ys => sort(xs.take(xs.size / 2 + 1))
  //}


  def mSort(xs: List[Int]): List[Int] = {
    val n = xs.length / 2
    if (n == 0) xs
    else {

      val (fst, snd) = xs.splitAt(n)
      merge(mSort(fst), mSort(snd))
    }

    def merge(xs: List[Int], ys: List[Int]): List[Int] = (xs, ys) match {
      case (Nil, ys) => ys
      case (xs, Nil) => xs
      case (x1 :: xs1, y1 :: ys1) =>
        if (x1 < y1) x1 :: merge(xs1, ys)
        else y1 :: merge(xs, ys1)
      case (_,_) => xs
    }

  }


  //  def merge2(ys: List[Int], zs: List[Int]): List[Int] = {
  //      if (ys.length == 0) zs
  //      else if (zs.length == 0) ys
  //      else {
  //        if (ys.head < zs.head) ys.head :: merge(ys.tail, zs)
  //        else /*if (zs.head < ys.head)*/ zs.head :: merge(ys, zs.tail)
  //      }
  //  }

  //  def merge3(ys: List[Int], zs: List[Int]): List[Int] = {
  ////    @tailrec
  //    def mergeAcc(ys: List[Int], zs: List[Int], acc: List[Int]): List[Int] = {
  //      if (ys.length == zs.length == 0) acc
  //      else if (ys.length == 0) acc ++ zs
  //      else if (zs.length == 0) acc ++ ys
  //      else {
  //        if (ys.head < zs.head)  mergeAcc(ys.tail, zs, ys.head :: acc)
  //        else  mergeAcc(ys, zs.tail, zs.head :: acc)
  //      }
  //    }
  //
  //    mergeAcc(ys, zs, List())
  //  }

  //  def merge(ys: List[Int], zs: List[Int]): List[Int] = ys match {
  //    case Nil => zs
  //    case x :: xs => ys match  {
  //      case Nil => ys
  //      case x1 :: xs1 => if (x1 < x) x1 :: merge(xs, ys)
  //          else x1 :: merge(ys, xs1)
  //    }
  //  }
  val nums = List(1, 2, 5, 43, 24, 12, 6, 1)

  mSort(nums)
}


