package quickcheck

import common._
import org.scalacheck._
import Arbitrary._
import Gen._
import Prop._
import scala.util.Random

abstract class QuickCheckHeap extends Properties("Heap") with IntHeap {

  

//
//If you insert an element into an empty heap,
//then delete the minimum, the resulting heap should be empty.
  property("insertDelete") = forAll { a: Int =>
    deleteMin(insert(a, empty)) == empty    
  }  
  
//Given any heap, you should get a sorted sequence of elements
//when continually finding and deleting minima. (Hint: recursion and helper functions are your friends.)

  
//Finding a minimum of the melding of any two heaps should return a minimum of one or the other
  property("meldingConsistency") = forAll { (h1: H, h2: H) =>
    findMin(meld(h1, h2)) == Math.min(findMin(h1), findMin(h2))
  }
  
  // given
  property("min1") = forAll { a: Int =>
    val h = insert(a, empty)
    findMin(h) == a
  }  
  
  // copy by meld with empty
  property("meldWithEmptyInvariance") = forAll { h: H =>
    meld(h, empty) == h
  }
  
  // TODO
//  property("meldWithSingleAkaInsert") = Prop.forAll(singleGenHeap) { (h1: H, h2: H) =>
//    meld(h1, h2) == insert(findMin(h2), h1)
//  }
  
  
  // minimum invariance 
  property("minInvariance") = forAll { (a: Int, h: H) =>
    val hCopy = meld(h, empty)
    val previousMin = findMin(hCopy)
    val h1 = insert(a, h)
    val actualMin = findMin(h1)
    
    Math.min(a, actualMin) == Math.min(a, previousMin)
  }
  
  //  If you insert any two elements into an empty heap,
  //finding the minimum of the resulting heap should get the smallest of the two elements back.
  property("minBack") = forAll { (a: Int, b:Int) =>
    val h = insert(b, insert(a, empty))
    findMin(h) == Math.min(a, b)
  }
  
  lazy val singleGenHeap: Gen[H] = {
    // TODO
    insert(Random.nextInt(), empty)
       
  }

  lazy val genHeap: Gen[H] = {
    // TODO
    insert(Random.nextInt(), empty)
    
    
  }
  
  

  implicit lazy val arbHeap: Arbitrary[H] = Arbitrary(genHeap)

}
