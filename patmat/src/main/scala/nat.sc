abstract class Nat {
  def isZero: Boolean
  def predecessor: Nat
  def successor: Nat = new Succ(this)
  def +(x: Nat): Nat
  def -(x: Nat): Nat
}
////
object Zero extends Nat {
  def isZero = true
  def + (x: Nat) = x
  def - (x: Nat) =
    if (x.isZero) this
    else throw new Error("negative")
  def predecessor: Nat = throw new Error("negative")
}

class Succ(n: Nat) extends Nat {
  override def isZero: Boolean = false
  override def + (x: Nat): Nat = new Succ(n + x)
  override def - (x: Nat): Nat =
    if (x.isZero) this
    else n - x.predecessor
  override def predecessor: Nat = n
}
////
//
