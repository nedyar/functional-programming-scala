package patmat

import patmat.Huffman._

/**
 * Created by Stefano on 05/10/2014.
 */
object Main {
  def main(args: Array[String]) {
    val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
    val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)

    val chars = string2Chars("aaaaassdddppdlas")
    val charsSorted = chars.sortWith(_ < _)
    println(charsSorted)

    val pairs = times(string2Chars("aababbbdafdddsa"))
    println(pairs)

    val ascending = makeOrderedListByWeight(pairs)
    println(ascending)

    val leafs = makeOrderedLeafList(pairs)
    println(leafs)

    println("insertion sorted2")
    println(insert(leafs, Leaf('z', 3)))

    val mergedChars = mergeChars(t1, t2)
    println(mergedChars)

    println(sumsWeights(t1, t2))

    println("combine")
    println(combine(makeOrderedLeafList(times(chars))))
    val one: List[CodeTree] = combine(makeOrderedLeafList(times(chars)))
    println(one)

    println("combine Nil")
    println(combine(Nil))

    println(Fork(one.head, one.tail.head, 'a' :: List(), -1))


    // FIXME mi da errore
//    println("create code")
//    println(createCodeTree(string2Chars("aabadjaskldjaslllllllbbbd")))

    decodePrinter()
  }

  def decodePrinter() = {
    println("create code")
    val tree = createCodeTree(string2Chars("aaaabcc"))
    println(tree)

    println("convert")
    println(convert(tree))

    println("bits")
    val secret: List[Bit] = List(0,0,1,0,0)
    println(secret)

    println("explore")
    println(explore(tree, secret))

    println("decode")
    println(decode(tree, secret))

    println("french secret")
    println(decodedSecret)

    println("contains a")
    println(containsChar(tree, 'a'))

    println("contains z")
    println(containsChar(tree, 'z'))

    val tree2 = createCodeTree(string2Chars("aaaabbffc"))
    println("tree2")
    println(tree2)

    println("convert tree2")
    println(convert(tree2))

    println("encode")
    println(encode(tree2)("aaabff".toList))



    println("quick encode")
    println(quickEncode(tree2)("aaabff".toList))

  }

}
