trait Expr
case class Number(n: Int) extends Expr
case class Sum(l: Expr, r: Expr) extends Expr
case class Var(x: String) extends Expr
case class Prod(l: Expr, r: Expr) extends Expr

object UtilityExpr{

  def eval(e: Expr): Int = e match {
    case Number(n) => n
    case Sum(l, r) => eval(l) + eval(r)
//    case Var(x) => x
//    case
  }

  def show(e: Expr): String = e match {
    case Number(n) => "" + n
    case Sum(l, r) => "(" + show(l) + " + " + show(r) + ")"
    case Var(x) => x
    case Prod(l, r) => show(l) + " * " + show(r)
  }

  val e = Sum(Number(1), Sum(Number(2), Number(3)))
  println(show(e))

  val e2 = Sum(e,e)
  show(e2)

  val e3 = Prod(e,e)
  show(e3)

  val e4 = Prod(Number(1), Var("x"))
  show(e4)

  val e5 = Sum(e4,e4)
  show(e5)
}