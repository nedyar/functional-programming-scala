package streams

import common._
import scala.annotation.tailrec

/**
 * This component implements the solver for the Bloxorz game
 */
trait Solver extends GameDef {

  /**
   * Returns `true` if the block `b` is at the final position
   */
  def done(b: Block): Boolean = b.isStanding && b.b1 == goal
  
  /**
   * This function takes two arguments: the current block `b` and
   * a list of moves `history` that was required to reach the
   * position of `b`.
   * 
   * The `head` element of the `history` list is the latest move
   * that was executed, i.e. the last move that was performed for
   * the block to end up at position `b`.
   * 
   * The function returns a stream of pairs: the first element of
   * the each pair is a neighboring block, and the second element
   * is the augmented history of moves required to reach this block.
   * 
   * It should only return valid neighbors, i.e. block positions
   * that are inside the terrain.
   */
  def neighborsWithHistory(b: Block, history: List[Move]): Stream[(Block, List[Move])] = {
    
    @tailrec
    def neighborsAcc(n: List[(Block, Move)], acc: Stream[(Block, List[Move])]): Stream[(Block, List[Move])] = {
      if (n.isEmpty) acc
      else neighborsAcc(n.tail, (n.head._1, n.head._2 :: history) #:: acc)  
    } 
    
    val legalNeighbors = b.legalNeighbors
    
    // XXX Is it lazy??
    neighborsAcc(legalNeighbors, Stream())
//    Stream((legalNeighbors.head._1, legalNeighbors.head._2 :: history))
    
//    lazy val neigh = legalNeighbors.map(pair => (pair._1, pair._2 :: history))

    // XXX it's correct?! (or it's been evaluated then discarted?
    // FIXME never ends when deal with infinite collection, so it isn't lazy at all!
//    neigh.toStream
  }

  /**
   * This function returns the list of neighbors without the block
   * positions that have already been explored. We will use it to
   * make sure that we don't explore circular paths.
   */
  def newNeighborsOnly(neighbors: Stream[(Block, List[Move])],
                       explored: Set[Block]): Stream[(Block, List[Move])] = {
    // XXX filter on Stream break benefits of lazyness? (no should be good)
    neighbors.filter(pair => !explored.contains(pair._1))
  }

  /**
   * The function `from` returns the stream of all possible paths
   * that can be followed, starting at the `head` of the `initial`
   * stream.
   * 
   * The blocks in the stream `initial` are sorted by ascending path
   * length: the block positions with the shortest paths (length of
   * move list) are at the head of the stream.
   * 
   * The parameter `explored` is a set of block positions that have
   * been visited before, on the path to any of the blocks in the
   * stream `initial`. When search reaches a block that has already
   * been explored before, that position should not be included a
   * second time to avoid cycles.
   * 
   * The resulting stream should be sorted by ascending path length,
   * i.e. the block positions that can be reached with the fewest
   * amount of moves should appear first in the stream.
   * 
   * Note: the solution should not look at or compare the lengths
   * of different paths - the implementation should naturally
   * construct the correctly sorted stream.
   */
  def from(initial: Stream[(Block, List[Move])],
           explored: Set[Block]): Stream[(Block, List[Move])] = {    
    if (initial.isEmpty) Stream.empty
    else {
      val more = for {
        elem <- initial
    	  neighbbors = neighborsWithHistory(elem._1, elem._2)
    	  if (!explored.contains(elem._1))
    	  next <- newNeighborsOnly(neighbbors, explored) 
      } yield next
      initial ++ from(more, explored ++ initial.map(p => p._1))
    }
  }
  
  // FIXME it istn't lazy bacause sortedInsertion it is not
  def directSolution(): List[Move] = {    
  
    @tailrec
    def explore(initial: Stream[(Block, List[Move])],
           explored: Set[Block]): List[Move] = {    
      if (initial.isEmpty) Nil
      else {
        val neighbbors = neighborsWithHistory(initial.head._1, initial.head._2)
        val newNeighbors = newNeighborsOnly(neighbbors, explored)
        
        val sol = newNeighbors.filter(p => p._1 == Block(goal, goal))
        if (!sol.isEmpty)
           sol.head._2.reverse
        else {     
          val sorted = sortedInsertion(newNeighbors, initial.tail)        
          explore(sorted, explored + initial.head._1)
        }
  //      sortedInsertion(from(newNeighbors, explored + initial.head._1), initial.tail)
      }
    }
    
    explore(Stream((startBlock, Nil)), Set())    
  }
  
  
//  def sortedInsertion(stream: Stream[(Block, List[Move])], olds: Stream[(Block, List[Move])]): Stream[(Block, List[Move])] = {
//    if (stream.isEmpty) olds
//    else if (olds.isEmpty) stream
//    else {
//      // assumption all the couples in stream have the same length (not used)
//      val insertLegth = stream.head._2.length
//      val headLength = olds.head._2.length
//      if (insertLegth <= headLength) stream.head #:: sortedInsertion(stream.tail, olds)
//      else olds.head #:: sortedInsertion(stream, olds.tail)
//    }
//  }
  def sortedInsertion(left: Stream[(Block, List[Move])], right: Stream[(Block, List[Move])]): Stream[(Block, List[Move])] = {
    if (left.isEmpty && right.isEmpty) Stream.empty
    else if (left.isEmpty) right.head #:: sortedInsertion(left, right.tail)
    else if (right.isEmpty) left.head #:: sortedInsertion(left.tail, right)
    else {
      val leftHeadLength = left.head._2.length
      val rightHeadLength = right.head._2.length
      if (leftHeadLength <= rightHeadLength) left.head #:: sortedInsertion(left.tail, right)
      else right.head #:: sortedInsertion(left, right.tail)
    }
  }
  
  
//  def sortedInsertion(stream: Stream[(Block, List[Move])], olds: Stream[(Block, List[Move])]): Stream[(Block, List[Move])] = {
//    @tailrec
//    def sortedInsertionAcc(left: Stream[(Block, List[Move])], right: Stream[(Block, List[Move])], acc: Stream[(Block, List[Move])]): Stream[(Block, List[Move])] = {
//      if (left.isEmpty && right.isEmpty) acc.reverse
//      else if (left.isEmpty) sortedInsertionAcc(left, right.tail, right.head #:: acc)
//      else if (right.isEmpty) sortedInsertionAcc(left.tail, right, left.head #:: acc)
//      else {
//        val leftHeadLength = left.head._2.length
//        val rightHeadLength = right.head._2.length
//        if (leftHeadLength <= rightHeadLength) sortedInsertionAcc(left.tail, right, left.head #:: acc)
//        else sortedInsertionAcc(left, right.tail, right.head #:: acc)
//      }
//    }
//    
//    sortedInsertionAcc(stream, olds, Stream())
//  }

  /**
   * The stream of all paths that begin at the starting block.
   */
  lazy val pathsFromStart: Stream[(Block, List[Move])] = {
    from(Stream((startBlock, List())), Set())
  }

  /**
   * Returns a stream of all possible pairs of the goal block along
   * with the history how it was reached.
   */
  lazy val pathsToGoal: Stream[(Block, List[Move])] = {
    pathsFromStart.filter(p => p._1 == Block(goal, goal))
  }

  /**
   * The (or one of the) shortest sequence(s) of moves to reach the
   * goal. If the goal cannot be reached, the empty list is returned.
   *
   * Note: the `head` element of the returned list should represent
   * the first move that the player should perform from the starting
   * position.
   */
  lazy val solution: List[Move] = {
    if (pathsToGoal.isEmpty) List()
    else pathsToGoal.head._2
  }
}
