package streams


object MainBloxorz {}
  
  
//  def main(args: Array[String]): Unit = {
//    new Level2 {
//      println(" ")
//      println("### test lazyness ###")
//      def successors(n: Int): Stream[Int] = {
//        n #:: successors(n + 1)
//      } 
//      
//      lazy val natural = successors(0)
//      lazy val even = natural.filter(i => i % 2 == 0)
//      
//      println("natural numbers: " + natural)
//      println("natural numbers to 10: " + natural.take(10).toList)
//      
//      println("even numbers: " + even)
//      println("even numbers to 10: " + even.take(10).toList)
//      
//      println(" ")
//      println("### LEVEL 2 ###")   
//      
//      //println(terrain(Pos(0,5)))
//      println(startPos)
//      println(goal)
//      println(goal)
//      val block1 = Block(Pos(1,2), Pos(1,2))
//      println("### block 1 ###")
//      println("is standing: " + block1.isStanding)
//      println("is legal: " + block1.isLegal)
//      println("neighbors: " + block1.neighbors)
//      println("legalNeighbors: " + block1.legalNeighbors)
//      
//      val block2 = Block(Pos(4,1), Pos(4,2))
//      println("### block 2 ###")
//      println("is standing: " + block2.isStanding)
//      println("is legal: " + block2.isLegal)
//      println("neighbors: " + block2.neighbors)
//      println("legalNeighbors: " + block2.legalNeighbors)
//      
//      println(" ")
//      println("### solving ###")      
//      println("neighborsWithHistory: " + neighborsWithHistory(block1, List(Right, Right, Down, Left, Left, Up).reverse))
//      println("neighborsWithHistory list: " + neighborsWithHistory(block1, List(Right, Right, Down, Left, Left, Up).reverse).toList)
//      
//      println("newNeighborsWithHistory: " +
//          newNeighborsOnly(neighborsWithHistory(block1, List(Right, Right, Down, Left, Left, Up).reverse),
//              Set(Block(Pos(2,2), Pos(3,2)))))
//      println("newNeighborsWithHistory list: " +
//          newNeighborsOnly(neighborsWithHistory(block1, List(Right, Right, Down, Left, Left, Up).reverse),
//              Set(Block(Pos(2,2), Pos(3,2)))).toList)  
//              
//      println("legal neigh of starting block: " + startBlock.legalNeighbors)       
//          
//      println("from start: " + pathsFromStart)
//      println("from start list: " + pathsFromStart.take(10).toList)
//      
//      // FIXME doesn't lead to optimal solution
//      println("direct solution: " + directSolution)      
//      //assert(directSolution == optsolution)
//      
////      println("sorted insertion test:" + 
////          sortedInsertion(
////              newNeighborsOnly(Stream((startBlock, List(Down))), Set()),
////              Stream((block1, List()), (block2, List(Right, Left)))
////          ).toList
////      )
//      
////      println("goal: " + goal)
////      
////      println("from start: " + pathsToGoal)
////      println("from start list: " + pathsToGoal.take(10).toList)
//      
//    }
//    
//	  new Level3{
//		  println(" ")
//      println("### LEVEL 3 ###")   
//		  println("from start: " + pathsFromStart)
//		  
//		  println("from start list: " + pathsFromStart.take(10).toList)
//		  
////		  println("state B(P(1,1),P(1,1)): " + pathsFromStart.filter(p => p._1 == Block(Pos(1,1), Pos(1,1))).take(1).toList)
//		  println("state B(P(1,1),P(1,1)): " + pathsFromStart.filter(p => p._1 == Block(Pos(1,1), Pos(1,1))))
////		  println("from start: " + pathsToGoal)
////		  println("from start list: " + pathsToGoal.take(10).toList)
//		  
//		  println("direct solution: " + directSolution)
//		  
//    
//      println("goal: " + goal)
//      
//      println("to goal: " + pathsToGoal)
//      println("to goal list: " + pathsToGoal.take(10).toList)
//		  println("solution: " + solution)
//	  }
//		  
//		  
//  }  
//}
//
// trait Level2 extends GameDef with Solver with StringParserTerrain {
//      /* terrain for level 2*/
//
//    val level =
//    """ooo-------
//      |oSoooo----
//      |ooooooooo-
//      |-ooooooooo
//      |-----ooToo
//      |------ooo-""".stripMargin
//
//    val optsolution = List(Right, Right, Down, Right, Right, Right, Down)
//  }
// 
//  trait Level3 extends GameDef with Solver with StringParserTerrain {
//      /* terrain for level 3*/
//
//    val level =
//    """ooo-------
//      |oSoooo----
//      |ooooTo----
//      |-o--------""".stripMargin
//
//    val optsolution = List(Right, Down, Right)
//  }
// 
 
