package pouring

object MainPouring {
  def main(args: Array[String]): Unit = {
    val problem = new Pouring(Vector(4, 7))
    
    println(problem.moves)
    
    println(problem.pathSets.take(3).toList)
  }
}