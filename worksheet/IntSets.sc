

abstract  class  IntSet {
  def incl(x: Int): IntSet
  def contains(x: Int): Boolean

  def union(other: IntSet): IntSet
}

object Empty extends IntSet{
  override def incl(x: Int): IntSet =
    new NonEmpty(x, Empty, Empty)

  override def contains(x: Int): Boolean = false
  override def toString = "."

  override def union(other: IntSet): IntSet = other
}

class NonEmpty(elem: Int,
               left: IntSet, right: IntSet)
  extends IntSet {

  override def incl(x: Int): IntSet = {
    if (x < elem) new NonEmpty(elem, left incl x, right)
    else if (x > elem)  new NonEmpty(elem, left, right incl x)
    else this
  }

  override def contains(x: Int): Boolean = {
    if (x < elem) left contains x
    else if (x > elem) right contains x
    else true
  }
  override def toString = "{" + left + elem + right + "}"

  override def union(other: IntSet): IntSet = {
    ((left union right) union other) incl elem
  }
}
//
//object  intsets {
//  val t1 = new NonEmpty(3, Empty, Empty)
//  val t2 = t1 incl 4
//
//  def error(msg: String) = throw new Error(msg)
//
//  error("nooo")
//}

//abstract class Base {
//  def foo = 1
//  def bar: Int
//}
//
//class Sub extends  Base {
//  override def foo = 2
//  def bar = 3
//}
//
//trait Planar {
//  def height: Int
//  def weight: Int
//  def surface = height * weight
//}

//class Square extends Shape with Planar with Movable {
//  override def height: Int = ???
//
//  override def weight: Int = ???
//}

//class Shape {
//
//}
//
//trait Movable {
//
//}