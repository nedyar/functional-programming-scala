trait List2[+T] {
  def isEmpty: Boolean
  def head: T
  def tail: List2[T]
  def prep[U >: T](elem: U): List2[U] = new Cons(elem, this)
}
//
class Cons[T](val head: T, val tail: List2[T]) extends List2[T] {
  def isEmpty = false
}
//
object Nil extends List2[Nothing]{
  def isEmpty = true
  def head: Nothing = throw  new NoSuchElementException("Nil.head")
  def tail: Nothing = throw  new NoSuchElementException("Nil.tail")
}

//object Test {
//  val l: List2[String] = Nil
 // def f(xs: List2[Cons], x: Empty) = xs prep x
//}

//
//object List2{
//  //def emptyList: List2
//
//  def apply[T](x1: T, x2: T): List2[T] = new Cons(x1, new Cons(x2, new Nil))
//}


// 4.3
// Liskov substitute principle
//def assertAllPos(s: IntSet): IntSet


// Objects Everywhere



abstract class Bool {
  def ifThenElse[T](t: => T, e: => T): T

  def &&(x: Bool): Bool = ifThenElse(x, Fa)
  def ||(x: Bool): Bool = ifThenElse(Tr, x)
  def unary_! : Bool = ifThenElse(Fa, Tr)

  def ==(x: Bool): Bool = ifThenElse(x, x.unary_!)
  def !=(x: Bool): Bool = ifThenElse(x.unary_!, x)

  //def < (x: Bool): Bool = ifThenElse()

  object Fa extends Bool {
    def ifThenElse[T](t: => T, e: => T): T = e
  }

  object Tr extends Bool {
    def ifThenElse[T](t: => T, e: => T): T = t
  }

  val tru = Tr
  val fal = Fa

//  def printAnd() = {  }
//  println(tru && fal)





    //  def init[T](xs: List2[T]): List2[T] = xs match {
    //    case List2() => throw  new Error()
    //    case List2(y) => List2()
    //    case y :: ys => y :: init(ys)
    //  }
    //
    //
//    def concat[T](xs: List2[T], ys: List2[T]) = xs match {
//      case List2() => ys
//      case z :: zs => z :: concat(zs, ys)
//    }
//    //
//    def reverse[T](xs: List2[T]): List2[T] = xs match {
//      case List2() => throw  new Error()
//      case List2(y) => xs
//      case y :: ys => reverse(ys) ++ List2(y)
//    }

    //  def removeAt[T](n: Int, xs: List2[T]) = match





}





