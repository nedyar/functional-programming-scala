object Ex {
  val x = new Rational(3,2)
  println(x.numerator)
  val y = new Rational(4,7)

  x + (y).toString

  val z = new Rational(1,5)
  val z2 = new Rational(-1,5)

  -z2
  z2 - (z)

  val a = new Rational(1,3)
  val b = new Rational(5,7)
  val c = new Rational(3,2)

  a - (b - (c))
  a - (b) - (c)

  val simp = new Rational(10,5)


  //(((a + b) ^? (c ?^ d))) less ((a ==> b) | c)
}


class Rational(val x: Int, val y: Int) {
  require(y != 0, "denominator must be nonzero")
//  this(numerator: Int, denominator: Int):Rational = {
//
//  }
  private val g = gdc(x, y)
  val numerator = x / g
  val denominator = y / g

//  val numerator = x
//  val denominator = y

  def this(x: Int) = this(x,1)

  private def gdc(a: Int, b: Int): Int =
    if (b==0) a else gdc(b, a % b)

  override def toString: String = {
    numerator + "/" + denominator
  }

//  override def toString: String = {
//    def g = gdc(numerator, denominator)
//    (numerator / g) + "/" + (denominator / g)
//  }


  def + (r: Rational): Rational = {
    new Rational(
      numerator * r.denominator + r.numerator * denominator,
      denominator * r.denominator)
  }

  def unary_- : Rational = {
    new Rational(-numerator, denominator)
  }

  def  - (r: Rational): Rational = {
    this + -r
  }

  def < (that: Rational) = numerator * that.denominator < that.numerator * denominator

  def max(that: Rational) = if (this < that) that else this




}




//def addRational(r: Rational, s: Rational): Rational ={
//  new Ratio
//}
