
val tolerance = 0.0001
def isCloseEnought(x: Double, y: Double) =
  math.abs((x-y)/x) / x < tolerance

def fixedPoint(f: Double => Double)(firstGuess: Double) = {
  def iterate(guess: Double): Double = {
    val next = f(guess)
    if (isCloseEnought(guess, next)) next
    else  iterate(next)
  }
  iterate(firstGuess)
}
fixedPoint(x => 1 + x/2)(1)

def averageDump(f: Double => Double)(x: Double) =
  (x + f(x)) / 2


def sqrt(x: Double) = fixedPoint(y => (y + x/y) / 2)(1)

def sqrt2(x: Double) = fixedPoint(averageDump(y => x/y))(1)

sqrt2(2)








