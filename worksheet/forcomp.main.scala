package forcomp

object main {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(74); 
  println("Welcome to the Scala worksheet");$skip(42); 
  
   val abba = List(('a', 2), ('b', 2));System.out.println("""abba  : List[(Char, Int)] = """ + $show(abba ));$skip(260); 
    val abbacomb = List(
      List(),
      List(('a', 1)),
      List(('a', 2)),
      List(('b', 1)),
      List(('a', 1), ('b', 1)),
      List(('a', 2), ('b', 1)),
      List(('b', 2)),
      List(('a', 1), ('b', 2)),
      List(('a', 2), ('b', 2))
    );System.out.println("""abbacomb  : List[List[(Char, Int)]] = """ + $show(abbacomb ));$skip(45); 
    val combin = Anagrams.combinations(abba);System.out.println("""combin  : List[forcomp.Anagrams.Occurrences] = """ + $show(combin ))}
    //println(combin)
}
