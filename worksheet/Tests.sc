def loop : Boolean = loop

//def and(x: Boolean, y: => Boolean): Boolean = {
//  if (x) y
//  else false
//}

def and(x: Boolean, y: => Boolean): Boolean = if (x) y else false

and(false, and(true, true))
and(true, and(true, true))
and(false, loop)

def or(x: Boolean, y: => Boolean): Boolean = if (x) true else y

or(false, or(true, true))
or(false, or(false, false))
  or(true, loop)
  or(true, loop)

def sqrt(x: Double): Double = {
  def sqrtIter(guess: Double): Double = {
    if (isGood(guess)) guess
    else sqrtIter(improve(guess))
  }

  //def isGood(guess: Double, x: Double): Boolean =
  //  Math.abs(guess * guess - x) < 0.001

  def isGood(guess: Double): Boolean =
    Math.abs(guess * guess - x) < x * 0.001


  def improve(guess: Double): Double =
    (x / guess + guess) / 2

  sqrtIter(1.0)
}

sqrt(2)
sqrt(1)
sqrt(3)
sqrt(4)
sqrt(1e-30)
sqrt(1e30)


def gcd(x: Int, y: Int) : Int =
  if (y == 0) x else gcd(y, x % y)

def fact(x: Int) : Int =
  if (x == 0) 1 else x * fact(x-1)

fact(6)
fact(1)

def fact2(x: Int) : Int = {
  def tailFact(x: Int, acc: Int) : Int =
  if (x == 0) acc else tailFact(x - 1, acc * x)

  tailFact(x, 1)
}

fact2(6)
fact2(1)





