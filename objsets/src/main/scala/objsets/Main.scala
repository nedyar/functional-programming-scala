package objsets

/**
 * Created by Stefano on 28/09/2014.
 */
object Main2 {
  def main(args: Array[String]) {
    val t1 = new Tweet("u1", "text1", 0)
    val t2 = new Tweet("u2", "text2", 2)
    val t3 = new Tweet("u2", "text3", 15)
    val t4 = new Tweet("u3", "text4", 3)

    val empty = new Empty()
    val set1 = new NonEmpty(t1, empty, empty)

    val tSet1 = new NonEmpty(t1, new NonEmpty(t2, new Empty, new Empty), new Empty)
    val tSet2 = tSet1.union(new NonEmpty(t3, new Empty, new Empty))
    val tSet3 = tSet2.union(new NonEmpty(t4, new Empty, new Empty))

    println("tSet1")
    tSet1 foreach println
    println("")

    println("tSet2")
    tSet2 foreach println
    println("")

    val tSet2F = tSet2.filter(tweet => tweet.retweets > 10)
    println("tSet2F")
    tSet2F foreach println
    println("")

    println("max ret2 " + tSet2.maxRetweet(-1))
    println("max ret3 " + tSet3.maxRetweet(-1))

    println("most ret2 " + tSet2.mostRetweeted)
    println("most ret3 " + tSet3.mostRetweeted)
    //println("most empty " + (new Empty).mostRetweeted)

    println("desc ")
    tSet3.descendingByRetweet foreach println
    //GoogleVsApple.trending foreach println

    val l1: TweetList = new Cons(tSet3.mostRetweeted, new Cons(tSet3.remove(tSet3.mostRetweeted).mostRetweeted, Nil))

    println("l1 ")
    l1 foreach println

    tweerReaderTests
  }


  def tweerReaderTests {
    val google = List("android", "Android", "galaxy", "Galaxy", "nexus", "Nexus")
    val apple = List("ios", "iOS", "iphone", "iPhone", "ipad", "iPad")

    lazy val googleTweets: TweetSet = {
      TweetReader.allTweets.filter(
        (t: Tweet) => google.exists((p: String) => t.text.contains(p))
      )
    }
    lazy val appleTweets: TweetSet = {
      TweetReader.allTweets.filter(
        (t: Tweet) => apple.exists((p: String) => t.text.contains(p)))
    }

    println("GOOGLE ")
    googleTweets foreach println

    println("GOOGLE SORTED")
    googleTweets.descendingByRetweet foreach println

    println("APPLE")
    appleTweets foreach println

    println("APPLE SORTED")
    appleTweets.descendingByRetweet foreach println

    lazy val trending: TweetList = appleTweets.union(googleTweets).descendingByRetweet
    println("TREND ")
    trending foreach println
  }
}
