import java.util.NoSuchElementException

trait List[T] {

  def isEmpty: Boolean
  def head: T

  def tail: List[T]

}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
  override def isEmpty: Boolean = false
}

class Nil[T] extends List[T] {
  override def isEmpty: Boolean = true

  def tail: Nothing = throw new NoSuchElementException("Nill.tail")

  def head: Nothing = throw new NoSuchElementException("Nill.head")
}

def singleton[T](elem: T) = new Cons[T](elem, new Nil[T])

def nth[T](n: Int, list: List[T]): T = {
  if (list.isEmpty) throw new IndexOutOfBoundsException("no way")
  //if (list == Nil || n < 0) throw new IndexOutOfBoundsException("no way")
  else if (n == 0) list head
  else nth(n - 1, list tail)
}

val list =  new Cons[Int](1, new Cons[Int](2, new Cons[Int](3, new Nil)))




