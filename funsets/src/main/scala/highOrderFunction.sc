def sum(f: Int => Int, a: Int, b: Int): Int = {
  if (a > b) 0
  else f(a) + sum(f, a+1, b)
}

sum((x: Int) => x, 2, 5)
sum((x: Int) => x * x * x, 2, 5)

def sumCubes(x: Int, y: Int): Int = sum(c => c*c*c,x,y)

sumCubes(2,3)

def sum2(f: Int => Int, a: Int, b: Int): Int = {
  def loop(a: Int, acc: Int): Int = {
    if (a > b) acc
    else loop(a + 1, acc + f(a))
  }
  loop(a, 0)
}

def sumCubes2(x: Int, y: Int): Int = sum2(c => c*c*c,x,y)
sumCubes2(2,3)


def sum3(f: Int => Int)(a: Int, b: Int): Int = {
  if (a > b) 0 else f(a) + sum3(f)(a + 1, b)
}

def product(f: Int => Int)(a: Int, b: Int): Int = {
  if (a > b) 1 else f(a) * product(f)(a+1, b)
}

def factorial(n: Int) = product(x => x)(1,n)

factorial(5)

def ps(o: (Int, Int) => Int, f: Int => Int, zero: Int)(a: Int, b: Int): Int = {
  if (a > b) zero else o(f(a),ps(o,f, zero)(a+1, b))
}

def pr(f: Int => Int)(a: Int, b: Int): Int = ps((x, y) => x * y, f,1)(a,b)

pr(x => x)(2,3)



