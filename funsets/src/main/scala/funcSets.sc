/**
 * 2. Purely Functional Sets.
 */
//object FunSets {
/**
 * We represent a set by its characteristic function, i.e.
 * its `contains` predicate.
 */
type Set = Int => Boolean

/**
 * Indicates whether a set contains a given element.
 */
def contains(s: Set, elem: Int): Boolean = s(elem)

/**
 * Returns the set of the one given element.
 */
def singletonSet(elem: Int): Set = (x: Int) => x == elem

/**
 * Returns the union of the two given sets,
 * the sets of all elements that are in either `s` or `t`.
 */
def union(s: Set, t: Set): Set = {
  (x: Int) => contains(s, x) || contains(t, x)
}

val s1 = singletonSet(1)
val s2 = singletonSet(2)
val s3 = singletonSet(3)
val s4 = singletonSet(4)

val s1_2 = union(s1, s2)
val s2_3 = union(s2, s3)
val s3_4 = union(s3, s4)
val s1_2_3_4a = union(s1_2, s3_4)
val s1_2_3_4b = union(s4, union(s1_2, s2_3))

toString(s1_2_3_4a)
toString(s1_2_3_4b)

toString(union(singletonSet(1), singletonSet(1)))



contains(s1_2, 3)
contains(s1_2, 1)
contains(s1_2, 2)

assert(contains(s1_2_3_4a, 1), "yep")
//assert(contains(s1_2_3_4a, 5), "nop")


/**
 * Returns the intersection of the two given sets,
 * the set of all elements that are both in `s` and `t`.
 */
def intersect(s: Set, t: Set): Set = (x: Int) => contains(s, x) && contains(t, x)

toString(intersect(s1_2, s2_3))


/**
 * Returns the difference of the two given sets,
 * the set of all elements of `s` that are not in `t`.
 */
def diff(s: Set, t: Set): Set = {
  (x: Int) => contains(s, x) && !contains(t, x)
}

toString(diff(s1_2_3_4a, s3))

/**
 * Returns the subset of `s` for which `p` holds.
 */
def filter(s: Set, p: Int => Boolean): Set = {
  (x: Int) => contains(s, x) && p(x)
}

toString(filter(s1_2_3_4a, (x: Int) => x > 3))



/**
 * The bounds for `forall` and `exists` are +/- 1000.
 */
val bound = 1000

/**
 * Returns whether all bounded integers within `s` satisfy `p`.
 */
def forall(s: Set, p: Int => Boolean): Boolean = {
  def iter(a: Int): Boolean = {
    if (a > bound) true
    else if (!p(a)) false
    else iter(a + 1)
  }
  iter(-bound)
}

val sT = union(singletonSet(5), union(singletonSet(6),
  union(singletonSet(7), singletonSet(8))))
var sTest = union(s1_2_3_4a, sT)
sTest = diff(sTest, s1)


toString(sTest)

def greater0 = (x: Int) => x >= 0
println("forall test: " + forall(sTest, greater0))
greater0(3)



////  /**
////   * Returns whether there exists a bounded integer within `s`
////   * that satisfies `p`.
////   */
////  def exists(s: Set, p: Int => Boolean): Boolean = ???
////
////  /**
////   * Returns a set transformed by applying `f` to each element of `s`.
////   */
////  def map(s: Set, f: Int => Int): Set = ???
//
/**
 * Displays the contents of a set
 */
def toString(s: Set): String = {
  val xs: IndexedSeq[Int] = for (i <- -bound to bound if contains(s, i)) yield i
  xs.mkString("{", ",", "}")
}

/**
 * Prints the contents of a set on the console.
 */
//def printSet(s: Set) {
//  println(toString(s))
//}
////}

