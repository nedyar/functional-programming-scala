package funsets

object Main extends App {
  import FunSets._

  println(contains(singletonSet(1), 1))
  println(singletonSet(5))
  println(contains(singletonSet(5), 3))
  println(contains(singletonSet(3), 3))

  val s1 = singletonSet(1)
  val s2 = singletonSet(2)
  val s3 = singletonSet(3)
  val s4 = singletonSet(4)

  val s1_2 = union(s1, s2)
  val s2_3 = union(s2, s3)
  val s3_4 = union(s3, s4)
  val s1_2_3_4a = union(s1_2, s3_4)
  val s1_2_3_4b = union(s4, union(s1_2, s2_3))

  printSet(s1_2_3_4a)
  printSet(s1_2_3_4b)

  printSet(union(singletonSet(1), singletonSet(1)))

  printSet(intersect(s1_2, s2_3))

  printSet(filter(s1_2_3_4a, (x: Int) => x > 3))

  contains(s1_2, 3)
  contains(s1_2, 1)
  contains(s1_2, 2)

  assert(contains(s1_2_3_4a, 1), "nop")
  //assert(contains(s1_2_3_4a, 5), "nop")

  contains(singletonSet(5), 3)
  contains(singletonSet(3), 3)


  printSet(diff(s1_2_3_4a, s3))
  
  val sT = union(singletonSet(5), union(singletonSet(6),
    union(singletonSet(7), singletonSet(8))))
  val sTest =  diff(union(s1_2_3_4a, sT), s1)

  def greaterThen(k: Int) = (x: Int) => x >= k
  printSet(sTest)
  println("forall test: " + forall(sTest, greaterThen(0)))
  greaterThen(0)(3)

  println("Exist:")
  println(exists(sTest, greaterThen(6)))
  println(exists(sTest, greaterThen(10)))

  println("MAP:")
  printSet(mapQuiteInverse(sTest, (x: Int) => x + x))
  printSet(map(union(s1_2_3_4a, singletonSet(15)), (x: Int) => x * 100))

  val l = List(1,2).map((x) => x + 1)
  println(l)
}
