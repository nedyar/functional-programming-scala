package forcomp

object Main {
  
  def main(args: Array[String]): Unit = {
    val abba = List(('a', 2), ('b', 2))
    val a = List(('a', 1))    
    val abbccccc = List(('a', 1), ('b', 2), ('c', 5))
    val aaabbcc = List(('a', 3), ('b', 2), ('c', 2))
    val aabb = List(('a', 2), ('b', 2))
    
    quickTests()
    

    val words = List("asd", "a", "b")
    println("remove from list: " + words.filterNot(_ == "asd"))
    
    println("combination of abba: " + Anagrams.combinations(abba))
    println("combination of a: " + Anagrams.combinations(a))
    println("combination of abbbc: " + Anagrams.combinations(abbccccc))    
    println("combination of Nil: " + Anagrams.combinations(Nil))
    
    println("reduce abba by (a,1): " + Anagrams.reduceByPairValue(abba, ('a', 1)))
    println("reduce abba by (a,2): " + Anagrams.reduceByPairValue(abba, ('a', 2)).filterNot(p => p._2 <= 0))
    
    println("abba - a: " + Anagrams.subtract(abba, a))
    println("abba - aabb: " + Anagrams.subtract(abba, aabb))
    
    val l = List(1,2,3)
    println("permutation of (1,2,3): " + l.permutations)
    //println("permutation of (1,2,3): " + SeqLike.permutation(l))
    val s = Set(1,2,3)
    println("subs (1,2,3): " + s.subsets.foreach(_.toList.permutations.foreach(print(_))))
    
    println("words having same occ of sane: " + Anagrams.wordsForOcc(Anagrams.wordOccurrences("sane")))
    //println("words having same occ of sane: " + Anagrams.wordsForOcc(Anagrams.wordOccurrences("sane")))
    
    anotherMethod()

  }
  
  def quickTests() = {
    
    
    println("eater subset of Heater: " + Anagrams.isOccSubset(Anagrams.wordOccurrences("Heather"), Anagrams.wordOccurrences("eater")))
    
    println("contains art? " + Anagrams.combinations(Anagrams.wordOccurrences("Heather")). exists(_ == List(('a',1), ('r',1), ('t',1))))
    println("words for heater: " + Anagrams.wordsForOccs(Anagrams.combinations(Anagrams.wordOccurrences("Heather"))))
    
    println("Heather anagrams: " + Anagrams.sentenceAnagrams(List("Heather")))
  }
  
  def anotherMethod(): Unit = {
        
//    val sa_me = List("sa", "me")
//    val sa_meOcc = sa_me.map(Anagrams.wordOccurrences(_))//List(Anagrams.wordOccurrences("sane"), Anagrams.wordOccurrences("my"))
//    println("words having same occ of sa and me : " + Anagrams.wordsForOccs(sa_meOcc))
//        
//    val occurrences2 = Anagrams.sentenceOccurrences(sa_me)
//    println("sentence occ of sa_me: " + occurrences2)
//    val comb2 = Anagrams.combinations(occurrences2)
//    println("sentence comb of sa_me: " + comb2)
//    println("sentence words for occ of sa_me: " + Anagrams.wordsForOccs(comb2).toSet)
//    
////    println("sentence anagrams2 of sa_me: " + Anagrams.sentenceAnagrams2(sa_me))
//    println("sentence anagrams of sa_me: " + Anagrams.sentenceAnagrams(sa_me))
//    
    
//    val sane_my = List("sane", "my")
//    val sane_myOcc = sane_my.map(Anagrams.wordOccurrences(_))//List(Anagrams.wordOccurrences("sane"), Anagrams.wordOccurrences("my"))
//    println("words having same occ of sane and my : " + Anagrams.wordsForOccs(sane_myOcc))
//        
//    val occurrences = Anagrams.sentenceOccurrences(sane_my)
//    println("sentence occ of sane_my: " + occurrences)
//    val comb = Anagrams.combinations(occurrences)
//    println("sentence comb of sane_my: " + comb)
//    println("sentence words for occ of sane_my: " + Anagrams.wordsForOccs(comb).toSet)
//
//    
//    println("sentence anagrams of sane_my: " + Anagrams.sentenceAnagrams(sane_my))
    
    //println("sentence anagrams: Linux rulez  " + Anagrams.sentenceAnagrams(List("linux", "rulez")))
//        = List("Linux", "rulez")
  }
  
}