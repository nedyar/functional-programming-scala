package forcomp

import common._
import scala.annotation.tailrec

object Anagrams {

  /** A word is simply a `String`. */
  type Word = String

  /** A sentence is a `List` of words. */
  type Sentence = List[Word]

  /** `Occurrences` is a `List` of pairs of characters and positive integers saying
   *  how often the character appears.
   *  This list is sorted alphabetically w.r.t. to the character in each pair.
   *  All characters in the occurrence list are lowercase.
   *  
   *  Any list of pairs of lowercase characters and their frequency which is not sorted
   *  is **not** an occurrence list.
   *  
   *  Note: If the frequency of some character is zero, then that character should not be
   *  in the list.
   */
  type Occurrences = List[(Char, Int)]

  /** The dictionary is simply a sequence of words.
   *  It is predefined and obtained as a sequence using the utility method `loadDictionary`.
   */
  val dictionary: List[Word] = loadDictionary

  /** Converts the word into its character occurence list.
   *  
   *  Note: the uppercase and lowercase version of the character are treated as the
   *  same character, and are represented as a lowercase character in the occurrence list.
   */
  def wordOccurrences(w: Word): Occurrences = 
     (for(e <- w) yield e.toLower).groupBy(e => e).map(m => (m._1, m._2.length())).toList.sorted

  /** Converts a sentence into its character occurrence list. */
  def sentenceOccurrences(s: Sentence): Occurrences = 
    wordOccurrences((for (w <- s) yield w).mkString)

  /** The `dictionaryByOccurrences` is a `Map` from different occurrences to a sequence of all
   *  the words that have that occurrence count.
   *  This map serves as an easy way to obtain all the anagrams of a word given its occurrence list.
   *  
   *  For example, the word "eat" has the following character occurrence list:
   *
   *     `List(('a', 1), ('e', 1), ('t', 1))`
   *
   *  Incidentally, so do the words "ate" and "tea".
   *
   *  This means that the `dictionaryByOccurrences` map will contain an entry:
   *
   *    List(('a', 1), ('e', 1), ('t', 1)) -> Seq("ate", "eat", "tea")
   *
   */
  // raggruppo le parole con lo stesso vettore occorrenze
  lazy val dictionaryByOccurrences: Map[Occurrences, List[Word]] = 
    dictionary.groupBy(w => wordOccurrences(w))

  /** Returns all the anagrams of a given word. */
  def wordAnagrams(word: Word): List[Word] = {
    dictionaryByOccurrences.get(wordOccurrences(word)) match {
       case Some(l) => l
       case None => List()
    }
  }

  /** Returns the list of all subsets of the occurrence list.
   *  This includes the occurrence itself, i.e. `List(('k', 1), ('o', 1))`
   *  is a subset of `List(('k', 1), ('o', 1))`.
   *  It also include the empty subset `List()`.
   * 
   *  Example: the subsets of the occurrence list `List(('a', 2), ('b', 2))` are:
   *
   *    List(
   *      List(),
   *      List(('a', 1)),
   *      List(('a', 2)),
   *      List(('b', 1)),
   *      List(('a', 1), ('b', 1)),
   *      List(('a', 2), ('b', 1)),
   *      List(('b', 2)),
   *      List(('a', 1), ('b', 2)),
   *      List(('a', 2), ('b', 2))
   *    )
   *
   *  Note that the order of the occurrence list subsets does not matter -- the subsets
   *  in the example above could have been displayed in some other order.
   */  
  def combinations(occurrences: Occurrences): List[Occurrences] = {
    def combinationsAcc(occurrences: Occurrences, acc: Set[Occurrences]): Set[Occurrences] = {
      if (occurrences == Nil) acc + List()
      else {
        (for {
          pair <- occurrences      
          occ <- combinationsAcc(reduce(occurrences, pair), acc + occurrences) 
          //if reduceable(pair)
//          if !acc.contains(occ)
        } yield occ).toSet
      }
    }    
    combinationsAcc(occurrences, Set()).toList   
  }
  
//  def combinationsT(occurrences: Occurrences): List[Occurrences] = {
//    
//    @tailrec
//    def combinationsAcc(occurrences: Occurrences, acc: List[Occurrences]): List[Occurrences] = {
//      if (occurrences == Nil) List() :: acc
//      else {
//        for {
//          pair <- occurrences      
//          occ <- combinationsAcc(reduce(occurrences, pair), occurrences :: acc) 
//          //if reduceable(pair)
////          if !acc.contains(occ)
//        } yield occ
//      }
//    }    
//    combinationsAcc(occurrences, List()).toList   
//  }
 
    
  def pairs(occurrences: Occurrences): List[Occurrences] = {
    for {
      pair <- occurrences     
      if (pair._2 != 2)
    } yield List(pair)
  }
   
  def reduce(o: Occurrences, pair: (Char, Int)): Occurrences = pair match {
    case (c, 1) => o.filterNot(p => p._1 == c)
    case (c, n) => o.map(pair => if (pair._1 == c) (pair._1, n - 1) else pair)
  }
  
  //def reduceable(pair: (Char, Int)): Boolean = pair match {
  //  case (_, 0) => false
  //  case (_, _) => true
  //}
      
  /** Subtracts occurrence list `y` from occurrence list `x`.
   * 
   *  The precondition is that the occurrence list `y` is a subset of
   *  the occurrence list `x` -- any character appearing in `y` must
   *  appear in `x`, and its frequency in `y` must be smaller or equal
   *  than its frequency in `x`.
   *
   *  Note: the resulting value is an occurrence - meaning it is sorted
   *  and has no zero-entries.
   */
  def subtract(x: Occurrences, y: Occurrences): Occurrences = {
    
    @tailrec
    def subAcc(x: Occurrences, y: Occurrences): Occurrences = y match {
      case Nil => x
      case y :: Nil => reduceByPairValue(x, y)
      case y :: ys => subAcc(reduceByPairValue(x, y), ys)
    }
    subAcc(x, y).filterNot(p => p._2 <= 0).sorted
  }
    
  def reduceByPairValue(o: Occurrences, pair: (Char, Int)): Occurrences = pair match {
    case (c, n) => o.map(e => if (e._1 == c) (e._1, e._2 - n) else e)
  }

  /** Returns a list of all anagram sentences of the given sentence.
   *  
   *  An anagram of a sentence is formed by taking the occurrences of all the characters of
   *  all the words in the sentence, and producing all possible combinations of words with those characters,
   *  such that the words have to be from the dictionary.
   *
   *  The number of words in the sentence and its anagrams does not have to correspond.
   *  For example, the sentence `List("I", "love", "you")` is an anagram of the sentence `List("You", "olive")`.
   *
   *  Also, two sentences with the same words but in a different order are considered two different anagrams.
   *  For example, sentences `List("You", "olive")` and `List("olive", "you")` are different anagrams of
   *  `List("I", "love", "you")`.
   *  
   *  Here is a full example of a sentence `List("Yes", "man")` and its anagrams for our dictionary:
   *
   *    List(
   *      List(en, as, my),
   *      List(en, my, as),
   *      List(man, yes),
   *      List(men, say),
   *      List(as, en, my),
   *      List(as, my, en),
   *      List(sane, my),
   *      List(Sean, my),
   *      List(my, en, as),
   *      List(my, as, en),
   *      List(my, sane),
   *      List(my, Sean),
   *      List(say, men),
   *      List(yes, man)
   *    )
   *
   *  The different sentences do not have to be output in the order shown above - any order is fine as long as
   *  all the anagrams are there. Every returned word has to exist in the dictionary.
   *  
   *  Note: in case that the words of the sentence are in the dictionary, then the sentence is the anagram of itself,
   *  so it has to be returned in this list.
   *
   *  Note: There is only one anagram of an empty sentence.
   */
  // Works but it's very slow!!!
  def sentenceAnagrams2(sentence: Sentence): List[Sentence] = {
    val occurrences = sentenceOccurrences(sentence)
    val comb = combinations(occurrences)
    val words = wordsForOccs(comb).toSet

    for {
      subset <- subsetOfOcc(comb)
      perm <- subset.toList.permutations
      if (sentenceOccurrences(perm) == occurrences)
    } yield subset.toList.distinct

  }

  // FIXME non mette alcune parole!
  def sentenceAnagrams(sentence: Sentence): List[Sentence] = {
    val occurrences = sentenceOccurrences(sentence)
    val comb = combinations(occurrences)
    val words = wordsForOccs(comb)//.toSet
    
    // improvement:
    // add words that have the same length of whole sentence occurrences and filter out from words 
    
    def anagrAcc(o: Occurrences, words: List[Word], partialSentence: Sentence, acc: List[Sentence]): List[Sentence] = {
      if (o.isEmpty) partialSentence :: acc
      else {
        for {
          w <- words
          wOcc = wordOccurrences(w)
          if isOccSubset(o, wOcc) // check subtract precondition
          sent <- anagrAcc(subtract(o, wOcc), words, w :: partialSentence, acc)
//          sent <- anagrAcc(subtract(o, wOcc), words.filterNot(_ == w), w :: partialSentence, acc)
        } yield sent   
//        if (words.isEmpty) acc
//        else if (isOccSubset(o, wordOccurrences(words.head))) acc ++ anagrAcc(subtract(o, wordOccurrences(words.head)), words.tail, words.head :: partialSentence, acc)
//        else acc ++ anagrAcc(o, words.tail, partialSentence, acc)
      }
    }     
    anagrAcc(occurrences, words, List(), List()) 
  }
    
  def subsetOfOcc(os: List[Occurrences]) = {
    wordsForOccs(os).toSet[String].subsets.toList
  }
  
  def wordsForOcc(o: Occurrences): List[Word] = {
    dictionaryByOccurrences.get(o) match {
     case Some(l) => l
     case None => List()
    }
  }
  
  def wordsForOccs(os: List[Occurrences]): List[Word] = {
    (for {
      o <- os
    } yield wordsForOcc(o)).flatten
  }
  
  /**
   * O(n^2) where n = max(sub.size, set.size)
   */
  def isOccSubset(set: Occurrences, sub: Occurrences): Boolean = {
    sub.forall(p => contained(set, p))
  }  
  /**
   * O(n) where n = set.size
   */
  def contained(set: Occurrences, pair: (Char, Int)): Boolean = {
    set.exists(p2 => (p2._1 == pair._1) && (p2._2 >= pair._2))
  }  

}
