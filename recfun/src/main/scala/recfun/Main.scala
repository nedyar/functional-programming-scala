package recfun
import scala.annotation.tailrec
import scala.collection.immutable.Stack

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }

    //println(countChange(6, List(2,3)))

    println(pascal(3,5))
  }

  /**
   * Exercise 1
   */
  // TODO use tail recursion!
  //def pascal(c: Int, r: Int): Int = ???
  def pascal(c: Int, r: Int): Int = {
    if (c > r) 0
    else if (c == 0) 1
    else if (c == r + 1) 1
    else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
   * Exercise 2
   */
 // def balance(chars: List[Char]): Boolean = ???
  def balance(chars: List[Char]): Boolean = {

    @tailrec
    def iterating(chars: List[Char], lastOpen: Boolean, numOpen: Integer, numClose: Integer): Boolean =
      if (chars.isEmpty) (!lastOpen && numClose == numOpen)
      else
        if (chars.head == '(') iterating(chars.tail, true, numOpen + 1, numClose)
        else if (chars.head == ')') iterating(chars.tail, false, numOpen, numClose + 1)
        else iterating(chars.tail, lastOpen, numOpen, numClose)

    iterating(chars, false, 0, 0)
  }
  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    @tailrec
    def combination(coins: List[Int], sum: Int,  comb: Int, lastCoinIndex: Int, stack: Stack[Int]): Int = {
      if (coins.isEmpty) comb
      else if (stack.isEmpty && lastCoinIndex != 0) combination(coins.tail, 0, comb, 0, new Stack[Int]())
      // init removing first coin
      else if (lastCoinIndex >= coins.size) combination(coins, sum - coins(stack.top), comb, stack.top + 1, stack.pop)
      else if (sum == money) combination(coins,  sum - coins(lastCoinIndex), comb + 1, lastCoinIndex + 1, stack.pop)
      else if (sum > money) combination(coins,  sum - coins(lastCoinIndex), comb, lastCoinIndex + 1, stack.pop)//BT
      else  combination(coins, sum + coins(lastCoinIndex), comb, lastCoinIndex, stack.push(lastCoinIndex))
    }

    if (money == 0 || coins.isEmpty) 0
    else combination(coins.sortWith(_ > _), 0, 0,0, new Stack[Int]())
  }
}
