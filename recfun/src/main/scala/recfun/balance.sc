import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer

def balance(chars: List[Char]): Boolean = {
  if (chars.isEmpty) true
  else if(chars.head == '(') findClose(chars.tail, ListBuffer())
  else true
}

//@tailrec
/* FIXME it fails because it catch the first closed ) that find
* so ((...)) evaluate like balance("(..") && balance(")") instead of
* take the last and evaluate balance("(...)") && balance("")
*/
def findClose(chars: List[Char], middle: ListBuffer[Char]): Boolean = {
  if (chars.isEmpty) false
  else if (chars.head == ')') balance(chars.tail) && balance(middle.toList)
  else findClose(chars.tail, middle += chars.head)
}

balance("()()".toList)

def balanceC(chars: List[Char]): Boolean = {

  @tailrec
  def iterating(chars: List[Char], lastOpen: Boolean, numOpen: Integer, numClose: Integer): Boolean =
    if (chars.isEmpty)
      (!lastOpen && numClose == numOpen)
    else
      if (chars.head == '(') iterating(chars.tail, true, numOpen + 1, numClose)
      else if (chars.head == ')') iterating(chars.tail, false, numOpen, numClose + 1)
      else iterating(chars.tail, lastOpen, numOpen, numClose)

  iterating(chars, false, 0, 0)
}

balanceC("".toList)
balanceC("()".toList)
balanceC("(())".toList)
balanceC("(()())".toList)
balanceC("(()))".toList)











