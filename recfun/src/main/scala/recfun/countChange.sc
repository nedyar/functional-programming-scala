import scala.annotation.tailrec
import scala.collection.immutable.Stack

def countChange(money: Int, coins: List[Int]): Int = {
  @tailrec
  def combination(coins: List[Int], sum: Int,  comb: Int, lastCoinIndex: Int, stack: Stack[Int]): Int = {
    if (coins.isEmpty) comb
    else if (stack.isEmpty && lastCoinIndex != 0) combination(coins.tail, 0, comb, 0, new Stack[Int]())
    // init removing first coin
    else if (lastCoinIndex >= coins.size) combination(coins, sum - coins(stack.top), comb, stack.top + 1, stack.pop)
    else if (sum == money) combination(coins,  sum - coins(lastCoinIndex), comb + 1, lastCoinIndex + 1, stack.pop)
    else if (sum > money) combination(coins,  sum - coins(lastCoinIndex), comb, lastCoinIndex + 1, stack.pop)//BT
    else  combination(coins, sum + coins(lastCoinIndex), comb, lastCoinIndex, stack.push(lastCoinIndex))
  }

  if (money == 0 || coins.isEmpty) 0
  else combination(coins.sortWith(_ > _), 0, 0,0, new Stack[Int]())
}

countChange(3, List(1,2)) // 2
countChange(4, List(2,4)) // 2
countChange(5, List(1,2,4)) // 4
countChange(10, List(1,2,4,5)) // 16
countChange(10, List(2,4,5)) // 4
countChange(10, List(1,2,4)) // 16

























